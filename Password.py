import re

def checkio(name):
    match_obj = re.match('[a-zA-Z0-9]+', name)
    search_obj_upper = re.search("[a-z]+", name)
    search_obj_lower = re.search("[A-Z]+", name)
    search_num = re.search("[0-9]+", name)
    if len(name) >= 10:
        if match_obj:
            if search_obj_upper and search_obj_lower and search_num:
                print(True)
            else:
                print(False)
        else:
            print(False)
    else:
        print(False)


checkio('nlnn243KKNbjk')